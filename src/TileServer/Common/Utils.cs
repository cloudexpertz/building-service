﻿using Microsoft.Data.Sqlite;
using Npgsql;
using System;
using System.Data.Common;
using System.Dynamic;
using System.IO;

namespace SI.GIS.BuildingServer.Common
{
    public static class Utils
    {
        public static readonly string ImagePng = "image/png";

        public static readonly string ImageJpeg = "image/jpeg";

        public static readonly string Protobuf = "application/x-protobuf";

        /// <summary>
        /// GetContentType
        /// </summary>
        /// <param name="tileFormat"></param>
        /// <returns></returns>
        public static string GetContentType(string tileFormat)
        {
            var mediaType = String.Empty;

            switch (tileFormat)
            {
                case "png":
                    mediaType = ImagePng;
                    break;
                case "jpg":
                    mediaType = ImageJpeg;
                    break;
                case "x-protobuf":
                    mediaType = Protobuf;
                    break;
                default:
                    throw new ArgumentException("tileFormat");
            }

            return mediaType;
        }

        /// <summary>
        /// ReverseY
        /// </summary>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static int ReverseY(int y, int z)
        {
            return Convert.ToInt32(Math.Pow(2, Convert.ToDouble(z))) - y - 1;
        }

        /// <summary>
        /// GetBytes
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static byte[] GetBytes(DbDataReader reader)
        {
            byte[] buffer = new byte[2048];

            using (MemoryStream stream = new MemoryStream())
            {
                long bytesRead = 0;
                long fieldOffset = 0;

                while ((bytesRead = reader.GetBytes(0, fieldOffset, buffer, 0, buffer.Length)) > 0)
                {
                    stream.Write(buffer, 0, (int)bytesRead);
                    fieldOffset += bytesRead;
                }

                return stream.ToArray();
            }
        }

        /// <summary>
        /// Gen uid "B06A3C7262A5453BB1A7FC7FB69053FD"
        /// </summary>
        /// <returns></returns>
        public static string GenUid()
        {
            return Guid.NewGuid().ToString("N").ToUpper();
        }

        /// <summary>
        /// Hàm chuyển đổi Tile X ra longitude
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Double Tile2long (int x, int z)
        {
            return x / (double)(1 << z) * 360.0 - 180;
        }

        /// <summary>
        /// Hàm chuyển đổi Tile Y ra latitude
        /// </summary>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Double Tile2lat (int y, int z)
        {
            double n = Math.PI - 2.0 * Math.PI * y / (double)(1 << z);
            return 180.0 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n)));
        }

        /// <summary>
        /// Hàm convert tile z/x/y sang bbox
        /// </summary>
        /// <param name="z"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static string GetBBoxFromTile(int z, int x, int y)
        {
            var SW_long = Utils.Tile2long(x, z).ToString();

            var SW_lat = Utils.Tile2lat(y + 1, z).ToString();

            var NE_long = Utils.Tile2long(x + 1, z).ToString();

            var NE_lat = Utils.Tile2lat(y, z).ToString();

            var bbox = SW_long + ',' + SW_lat + ',' + NE_long + ',' + NE_lat;

            return bbox;
        }

        /// <summary>
        /// Set color by height
        /// </summary>
        /// <param name="properties"></param>
        public static void SetColorBuilding(dynamic properties)
        {
            if (properties.height > 0)
            {
                if (properties.height < 5)
                {
                    properties.color = "#7F3B08";
                    properties.roofColor = "#7F3B08";
                }
                else
                {
                    if (properties.height < 10)
                    {
                        properties.color = "#A6763A";
                        properties.roofColor = "#FFF679";
                    }
                    else
                    {
                        if (properties.height < 20)
                        {
                            properties.color = "#E0E2EE";
                            properties.roofColor = "#FFFFFF";
                        }

                        else
                        {
                            if (properties.height < 30)
                            {
                                properties.roofColor = "#E0E2EE";
                                properties.color = "#FFFFFF";
                            }

                            else
                            {
                                if (properties.height < 50)
                                {
                                    properties.color = "#AAA2CC";
                                    properties.roofColor = "#EDE2FF";
                                }

                                else
                                {
                                    if (properties.height < 100)
                                    {
                                        properties.color = "#664797";
                                        properties.color = "#EDE2FF";
                                    }

                                    else
                                    {
                                        properties.color = "#2D004B";
                                        properties.color = "#EDE2FF";
                                    }
                                }
                            }
                        }
                    }

                }

            }
        }
    }
}
