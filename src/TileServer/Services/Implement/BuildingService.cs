﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Npgsql;
using SI.GIS.BuildingServer.Common;
using SI.GIS.BuildingServer.Infrastructure.DTO;
using SI.GIS.BuildingServer.Infrastructure.Model;
using SI.GIS.BuildingServer.Services.Interface;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Services.Implement
{
    public class BuildingService : IBuildingService
    {

        private readonly ILogger<BuildingService> _logger;
        private readonly DatabaseContext _databaseContext;
        private readonly IConfiguration _configuration;

        public BuildingService(ILogger<BuildingService> logger,
                                    DatabaseContext databaseContext,
                                    IConfiguration configuration)
        {
            _logger = logger;
            _databaseContext = databaseContext;
            _configuration = configuration;

        }

        /// <summary>
        /// Lấy dữ liệu building hiển thị 3D
        /// </summary>
        /// <param name="bbox"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public async Task<GeoJsonModel> GetBuilding3D(string bbox, int zoom)
        {
            try
            {
                String connectionString = _configuration.GetConnectionString("DefaultConnection");

                var result = new GeoJsonModel
                {
                    Features = new List<FeatureModel>()
                };

                string whereCondition = " shape_area > 100 AND ";
                if (zoom > 18)
                    whereCondition = " ";
                //if (zoom >= 16 && zoom < 18)
                //    whereCondition += " chieucao > 20 AND ";
                //if (zoom >= 18 && zoom < 19)
                //    whereCondition += " chieucao > 10 AND ";

                var sqlFilterBulding = "SELECT * FROM hanoi_building nhahn WHERE" + whereCondition + "ST_Contains(ST_MakeEnvelope(" + bbox + ", 4326), ST_Centroid(nhahn.geom))";

                var commandText = $"With temp as (SELECT gid,ST_AsText(geom) as geom, chieucao FROM ({ sqlFilterBulding}) as bb) SELECT gid, ST_AsGeoJSON(ST_GeomFromText(temp.geom, 4326)) As gjson, chieucao from temp";
                using (var connection = new NpgsqlConnection(connectionString))
                {
                    using (var command = new NpgsqlCommand(commandText, connection))
                    {

                        await connection.OpenAsync().ConfigureAwait(false);
                        using (var dr = await command.ExecuteReaderAsync().ConfigureAwait(false))
                        {
                            while (await dr.ReadAsync().ConfigureAwait(false))
                            {
                                var feature = new FeatureModel
                                {
                                    Id = dr["gid"].ToString()
                                };

                                var geom = JsonConvert.DeserializeObject<object>(dr["gjson"].ToString());

                                feature.Geometry = geom;
                                dynamic properties = new ExpandoObject();

                                properties.height = (int)(Decimal)dr["chieucao"];

                                Utils.SetColorBuilding(properties);

                                feature.Properties = properties;

                                result.Features.Add(feature);
                            }

                            dr.Close();
                        }
                    }

                    connection.Close();

                    return result;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        /// <summary>
        /// Lấy dữ liệu building hiển thị trên basemap
        /// </summary>
        /// <param name="bbox"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public async Task<GeoJsonModel> GetBuildingForBaseMap(string bbox, int zoom)
        {
            try
            {
                String connectionString = _configuration.GetConnectionString("DefaultConnection");

                var result = new GeoJsonModel
                {
                    Features = new List<FeatureModel>()
                };

                string whereCondition = " shape_area > 100 AND ";
                if (zoom > 18)
                    whereCondition = " ";


                var sqlFilterBulding = "SELECT * FROM hanoi_building nhahn WHERE" + whereCondition + "ST_Contains(ST_MakeEnvelope(" + bbox + ", 4326), ST_Centroid(nhahn.geom))";

                var commandText = $"With temp as (SELECT gid,ST_AsText(geom) as geom, chieucao FROM ({ sqlFilterBulding}) as bb) SELECT gid, ST_AsGeoJSON(ST_GeomFromText(temp.geom, 4326)) As gjson, chieucao from temp";
                using (var connection = new NpgsqlConnection(connectionString))
                {
                    using (var command = new NpgsqlCommand(commandText, connection))
                    {

                        await connection.OpenAsync().ConfigureAwait(false);
                        using (var dr = await command.ExecuteReaderAsync().ConfigureAwait(false))
                        {
                            while (await dr.ReadAsync().ConfigureAwait(false))
                            {
                                var feature = new FeatureModel
                                {
                                    Id = dr["gid"].ToString()
                                };

                                var geom = JsonConvert.DeserializeObject<object>(dr["gjson"].ToString());

                                feature.Geometry = geom;
                                dynamic properties = new ExpandoObject();

                                properties.height = (int)(Decimal)dr["chieucao"] / 10;

                                properties.color = "#CCCCCC";
                                properties.roofColor = "#E2E5EE";

                                feature.Properties = properties;

                                result.Features.Add(feature);
                            }

                            dr.Close();
                        }
                    }

                    connection.Close();

                    return result;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public async Task<byte[]> GetTileBuilding(string bbox)
        {
            String connectionString = _configuration.GetConnectionString("DefaultConnection");

            var commandText = "SELECT ST_AsMVT(mvtgeom.*) FROM(SELECT ST_AsMVTGeom(geom, ST_MakeEnvelope(" + bbox + ", 4326)) AS geom" + ",height, min_height" + " FROM " + "vn_building"
                + " WHERE ST_Intersects(geom,  ST_MakeEnvelope(" + bbox + "))"
                + ") mvtgeom";

            using (var connection = new NpgsqlConnection(connectionString))
            {

                using (var command = new NpgsqlCommand(commandText, connection))
                {
                    await connection.OpenAsync().ConfigureAwait(false);
                    using (var dr = await command.ExecuteReaderAsync().ConfigureAwait(false))
                    {
                        if (await dr.ReadAsync().ConfigureAwait(false))
                        {
                            var tileByte = Utils.GetBytes(dr);

                            return tileByte;
                        }

                        dr.Close();
                    }
                }

                connection.Close();
            }

            return null;
        }
    }
}
