﻿using SI.GIS.BuildingServer.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Services.Interface
{
    public interface IBuildingService
    {
        /// <summary>
        /// Lấy dữ liệu tòa nhà từ db sang geoJson hiển thị trên basemap
        /// </summary>
        /// <param name="bbox"></param>
        /// <returns></returns>
        Task<GeoJsonModel> GetBuildingForBaseMap(string bbox, int zoom);

        /// <summary>
        /// Lấy dữ liệu tòa nhà từ db sang geoJson hiển thị 2.5D
        /// </summary>
        /// <param name="bbox"></param>
        /// <returns></returns>
        Task<GeoJsonModel> GetBuilding3D(string bbox, int zoom);

        Task<byte[]> GetTileBuilding(string bbox);
    }
}
