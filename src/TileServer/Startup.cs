using Autofac;
using Autofac.Extensions.DependencyInjection;
using GIS.CQRS.Events;
using GIS.iNRES.Common;
using GIS.iNRES.Common.Mvc;
using GIS.Persistence.PostgreSQL;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SI.GIS.BuildingServer.Common.Middleware;
using SI.GIS.BuildingServer.Configurations;
using SI.GIS.BuildingServer.Services.Implement;
using SI.GIS.BuildingServer.Services.Interface;
using Swashbuckle.Swagger;
using System;
using System.IO.Compression;
using System.Linq;
using System.Reflection;

namespace SI.GIS.BuildingServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IContainer Container { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.Providers.Add<GzipCompressionProvider>();
                options.MimeTypes =
                    ResponseCompressionDefaults.MimeTypes.Concat(
                        new[] { "application/json", "application/x-protobuf" });
            });
            services.AddResponseCompression();

            // get url client
            var urlClient = Configuration.GetSection("UrlClients").Value.Split(',', StringSplitOptions.RemoveEmptyEntries);

            // Add CORS:
            services.AddCors();

            // add CustomizeMVC
            services.AddCustomMvc();

            // setup config connectionstring 
            services.AddConnectionString(Configuration);

            // AddControllers
            services.AddControllers();

            // add Reposioties
            services.AddPostgreSQLRepository();

            // add auto mapping

            // add Doamin Notification
            services.AddEventHandlers();

            services.AddSwaggerGen(config =>
            {
                config.OperationFilter<SwaggerHeaderFilter>();
            });

            ConfigureOptions(services);

            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly()).AsImplementedInterfaces();

            builder.Populate(services);

            Container = builder.Build();

            return new AutofacServiceProvider(Container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                               IWebHostEnvironment env,
                               IStartupInitializer startupInitializer,
                               ILoggerFactory loggerFactory)
        {
            app.UseResponseCompression();
            //
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            // application builder extension
            ApplicationBuilder(app, env, loggerFactory);

            // Use SwaggerDocs
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            startupInitializer.InitializeAsync();
        }

        /// <summary>
        /// ApplicationBuilder
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        private void ApplicationBuilder(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.SetupEnv(env, loggerFactory);
        }

        /// <summary>
        /// Get filename sqlite
        /// </summary>
        /// <param name="services"></param>
        private void ConfigureOptions(IServiceCollection services)
        {
            // add options
        }
    }
}
