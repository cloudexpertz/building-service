﻿using System;
using System.Collections.Generic;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Pointconfig
    {
        public int? Level { get; set; }
        public string Ordered { get; set; }
    }
}
