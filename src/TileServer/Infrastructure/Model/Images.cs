﻿using System;
using System.Collections.Generic;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Images
    {
        public byte[] TileData { get; set; }
        public string TileId { get; set; }
    }
}
