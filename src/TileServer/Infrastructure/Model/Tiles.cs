﻿using System;
using System.Collections.Generic;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Tiles
    {
        public int? ZoomLevel { get; set; }
        public int? TileColumn { get; set; }
        public int? TileRow { get; set; }
        public byte[] TileData { get; set; }
    }
}
