﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Khoinhahn
    {
        public int Gid { get; set; }
        public long? Objectid1 { get; set; }
        public long? Objectid { get; set; }
        public string Manhandang { get; set; }
        public DateTime? Ngaythunha { get; set; }
        public DateTime? Ngaycapnha { get; set; }
        public string Madoituong { get; set; }
        public long? Doituong { get; set; }
        public string Ten { get; set; }
        public decimal? Chieucao { get; set; }
        public decimal? ShapeLeng { get; set; }
        public decimal? HightInch { get; set; }
        public decimal? ShapeLe1 { get; set; }
        public decimal? ShapeArea { get; set; }
        public MultiPolygon Geom { get; set; }
    }
}
