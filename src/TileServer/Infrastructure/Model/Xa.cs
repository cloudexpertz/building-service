﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Xa
    {
        public int Gid { get; set; }
        public int? Objectid { get; set; }
        public string Name { get; set; }
        public int? Id { get; set; }
        public string Cityname { get; set; }
        public string Distname { get; set; }
        public decimal? Id1 { get; set; }
        public string Madvhc { get; set; }
        public string Tenvi { get; set; }
        public string Tenen { get; set; }
        public string Capdvhc { get; set; }
        public string Maxa { get; set; }
        public string Tenxa { get; set; }
        public string Mahuyen { get; set; }
        public string Tenhuyen { get; set; }
        public string Matinh { get; set; }
        public string Tentinh { get; set; }
        public decimal? ShapeLeng { get; set; }
        public decimal? ShapeArea { get; set; }
        public MultiPolygon Geom { get; set; }
        public int? ColorRank { get; set; }
    }
}
