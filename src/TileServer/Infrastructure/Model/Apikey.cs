﻿using System;
using System.Collections.Generic;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class Apikey
    {
        public DateTime? Expriry { get; set; }
        public string Key { get; set; }
        public int Id { get; set; }
    }
}
