﻿using System;
using System.Collections.Generic;

namespace SI.GIS.BuildingServer.Infrastructure.Model
{
    public partial class ClusterTb
    {
        public int? Cid { get; set; }
        public int? Gid { get; set; }
    }
}
