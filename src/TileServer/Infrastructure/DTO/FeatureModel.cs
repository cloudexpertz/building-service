﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Infrastructure.DTO
{
    public class FeatureModel
    {
        public string Id { get; set; }
        public ExpandoObject Properties { get; set; }
        public string Type { get; set; } = "Feature";
        public object Geometry { get; set; }

    }
}
